package GUI;

import DifferentialEquationSolving.Grid;

/***
 * Ways to output the value of solved Initial Value Problem
 */
public class Printer {

    public static void printConsole(Grid grid) {
        for (int i = 0; i < grid.getSize(); i++) {
            System.out.println(grid.getXat(i) + " " + grid.getYat(i));
        }
    }
}