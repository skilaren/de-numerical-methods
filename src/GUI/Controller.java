package GUI;

import DifferentialEquationSolving.*;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.event.ActionEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

import java.util.ArrayList;

public class Controller {

    @FXML
    private Button plotButton;
    @FXML
    private Tab solutionsTab;
    @FXML
    private Tab errorsTab;

    @FXML
    private Pane pane;
    @FXML
    private TabPane tabPane;

    @FXML
    private TextField x0Input;
    @FXML
    private TextField y0Input;
    @FXML
    private TextField xInput;
    @FXML
    private TextField stepsNumberInput;

    @FXML
    private Text x0IvpError;
    @FXML
    private Text y0IvpError;
    @FXML
    private Text xIvpError;
    @FXML
    private Text stepIvpError;

    @FXML
    private TextField nStartInput;
    @FXML
    private TextField nLastInput;

    @FXML
    private GridPane scene;

    @FXML
    private NumberAxis xAxis = new NumberAxis();
    @FXML
    private NumberAxis yAxis = new NumberAxis();
    @FXML
    private LineChart<Number, Number> chartAll = new LineChart<>(xAxis, yAxis);
    @FXML
    private LineChart<Number, Number> chartError = new LineChart<>(new NumberAxis(), new NumberAxis());
    @FXML
    private LineChart<Number, Number> globalErrorChart = new LineChart<>(new NumberAxis(), new NumberAxis());

    private ArrayList<XYChart.Series<Number, Number>> exactSeries = new ArrayList<>();
    private ArrayList<XYChart.Series<Number, Number>> eulerSeries = new ArrayList<>();
    private ArrayList<XYChart.Series<Number, Number>> improvedEulerSeries = new ArrayList<>();
    private ArrayList<XYChart.Series<Number, Number>> rungeKuttaSeries = new ArrayList<>();

    private ArrayList<XYChart.Series<Number, Number>> eulerLocalErrorSeries = new ArrayList<>();
    private ArrayList<XYChart.Series<Number, Number>> improvedEulerLocalErrorSeries = new ArrayList<>();
    private ArrayList<XYChart.Series<Number, Number>> rungeKuttaLocalErrorSeries = new ArrayList<>();

    private ArrayList<XYChart.Series<Number, Number>> eulerGlobalErrorSeries = new ArrayList<>();
    private ArrayList<XYChart.Series<Number, Number>> improvedEulerGlobalErrorSeries = new ArrayList<>();
    private ArrayList<XYChart.Series<Number, Number>> rungeKuttaGlobalErrorSeries = new ArrayList<>();

    @FXML
    private CheckBox exactChartCheckbox;
    @FXML
    private CheckBox eulerChartCheckbox;
    @FXML
    private CheckBox improvedEulerChartCheckbox;
    @FXML
    private CheckBox rungeKuttaChartCheckbox;
    @FXML
    private CheckBox eulerLocalErrorCheckbox;
    @FXML
    private CheckBox improvedEulerLocalErrorCheckbox;
    @FXML
    private CheckBox rungeKuttaLocalErrorCheckbox;
    @FXML
    private CheckBox eulerGlobalErrorCheckbox;
    @FXML
    private CheckBox improvedEulerGlobalErrorCheckbox;
    @FXML
    private CheckBox rungeKuttaGlobalErrorCheckbox;

    @FXML
    protected void plotChartPressed(ActionEvent event) {
        chartAll.getData().clear();
        chartError.getData().clear();
        while (!chartAll.getData().isEmpty() || !chartError.getData().isEmpty()) {
        }
        double x0 = Double.parseDouble(x0Input.getText());
        double y0 = Double.parseDouble(y0Input.getText());
        double x = Double.parseDouble(xInput.getText());
        int stepsNumber = Integer.parseInt(stepsNumberInput.getText());
        x0IvpError.setText(x0Input.getText());
        y0IvpError.setText(y0Input.getText());
        xIvpError.setText(xInput.getText());
        stepIvpError.setText(stepsNumberInput.getText());
        IVP ivp = new IVP(x0, y0, x, stepsNumber);
        Exact exact = new Exact(ivp);
        Euler euler = new Euler(ivp);
        ImprovedEuler improvedEuler = new ImprovedEuler(ivp);
        RungeKutta rungeKutta = new RungeKutta(ivp);
        ApproximationError exactEuler = new ApproximationError(exact, euler);
        ApproximationError exactImprovedEuler = new ApproximationError(exact, improvedEuler);
        ApproximationError exactRungeKutta = new ApproximationError(exact, rungeKutta);
        addData(chartAll, exact.getGrids(), exactSeries, exactChartCheckbox, "-fx-stroke: #BB22BB");
        addData(chartAll, euler.getGrids(), eulerSeries, eulerChartCheckbox, "-fx-stroke: #FF4444");
        addData(chartAll, improvedEuler.getGrids(), improvedEulerSeries, improvedEulerChartCheckbox, "-fx-stroke: #4444FF");
        addData(chartAll, rungeKutta.getGrids(), rungeKuttaSeries, rungeKuttaChartCheckbox, "-fx-stroke: #44FF44");
        addData(chartError, exactEuler.getErrorGridList(), eulerLocalErrorSeries, eulerLocalErrorCheckbox, "-fx-stroke: #DD3388");
        addData(chartError, exactImprovedEuler.getErrorGridList(), improvedEulerLocalErrorSeries, improvedEulerLocalErrorCheckbox, "-fx-stroke: #8833DD");
        addData(chartError, exactRungeKutta.getErrorGridList(), rungeKuttaLocalErrorSeries, improvedEulerLocalErrorCheckbox, "-fx-stroke: #888888");
    }

    @FXML
    protected void plotGlobalErrorPressed(ActionEvent event) {
        globalErrorChart.getData().clear();
        while (!globalErrorChart.getData().isEmpty()) {
        }
        int nStart = Integer.parseInt(nStartInput.getText());
        int nLast = Integer.parseInt(nLastInput.getText());
        double x0 = Double.parseDouble(x0Input.getText());
        double y0 = Double.parseDouble(y0Input.getText());
        double x = Double.parseDouble(xInput.getText());
        IVP ivp = new IVP(x0, y0, x, nStart);
        Euler euler = new Euler(ivp);
        ImprovedEuler improvedEuler = new ImprovedEuler(ivp);
        RungeKutta rungeKutta = new RungeKutta(ivp);
        GlobalApproximationError eulerGlobalError = new GlobalApproximationError(euler, nStart, nLast);
        GlobalApproximationError improvedEulerGlobalError = new GlobalApproximationError(improvedEuler, nStart, nLast);
        GlobalApproximationError rungeKuttaGlobalError = new GlobalApproximationError(rungeKutta, nStart, nLast);
        addData(globalErrorChart, eulerGlobalError.getGlobalErrors(), eulerGlobalErrorSeries, eulerGlobalErrorCheckbox, "-fx-stroke: #DD3388");
        addData(globalErrorChart, improvedEulerGlobalError.getGlobalErrors(), improvedEulerGlobalErrorSeries, improvedEulerGlobalErrorCheckbox, "-fx-stroke: #8833DD");
        addData(globalErrorChart, rungeKuttaGlobalError.getGlobalErrors(), rungeKuttaGlobalErrorSeries, rungeKuttaGlobalErrorCheckbox, "-fx-stroke: #888888");
    }

    @FXML
    protected void exactChartCheckboxPressed(ActionEvent event) {
        turnVisibility(exactChartCheckbox, exactSeries);
    }

    @FXML
    protected void eulerChartCheckboxPressed(ActionEvent event) {
        turnVisibility(eulerChartCheckbox, eulerSeries);
    }

    @FXML
    protected void improvedEulerChartCheckboxPressed(ActionEvent event) {
        turnVisibility(improvedEulerChartCheckbox, improvedEulerSeries);
    }

    @FXML
    protected void rungeKuttaChartCheckboxPressed(ActionEvent event) {
        turnVisibility(rungeKuttaChartCheckbox, rungeKuttaSeries);
    }

    @FXML
    protected void eulerLocalErrorCheckboxPressed(ActionEvent event) {
        turnVisibility(eulerLocalErrorCheckbox, eulerLocalErrorSeries);
    }

    @FXML
    protected void improvedEulerLocalErrorCheckboxPressed(ActionEvent event) {
        turnVisibility(improvedEulerLocalErrorCheckbox, improvedEulerLocalErrorSeries);
    }

    @FXML
    protected void rungeKuttaLocalErrorCheckboxPressed(ActionEvent event) {
        turnVisibility(rungeKuttaLocalErrorCheckbox, rungeKuttaLocalErrorSeries);
    }

    @FXML
    protected void eulerGlobalErrorCheckboxPressed(ActionEvent event) {
        turnVisibility(eulerGlobalErrorCheckbox, eulerGlobalErrorSeries);
    }

    @FXML
    protected void improvedEulerGlobalErrorCheckboxPressed(ActionEvent event) {
        turnVisibility(improvedEulerGlobalErrorCheckbox, improvedEulerGlobalErrorSeries);
    }

    @FXML
    protected void rungeKuttaGlobalErrorCheckboxPressed(ActionEvent event) {
        turnVisibility(rungeKuttaGlobalErrorCheckbox, rungeKuttaGlobalErrorSeries);
    }

    /***
     * turns visibility of all elements of series list depending on checkbox
     * @param checkBox checkbox to check
     * @param seriesList seriesList to set visible/invisible
     */
    private void turnVisibility(CheckBox checkBox, ArrayList<XYChart.Series<Number, Number>> seriesList) {
        if (!checkBox.isSelected()) {
            for (XYChart.Series<Number, Number> series : seriesList) {
                series.getNode().setVisible(false);
            }
        } else {
            for (XYChart.Series<Number, Number> series : seriesList) {
                series.getNode().setVisible(true);
            }
        }
    }

    /***
     * add grids to series list and then to the chart if correspondent checkbox is selected with given style
     * @param chart chart where to add data
     * @param grids grids is the list of grids to add in the series
     * @param seriesList series where to add
     * @param checkBox coreespondent checkbox
     * @param style style you want to apply to the series
     */
    private void addData(LineChart<Number, Number> chart, ArrayList<Grid> grids, ArrayList<XYChart.Series<Number, Number>> seriesList, CheckBox checkBox, String style) {
        for (Grid grid : grids) {
            seriesList.add(new XYChart.Series<>());
            for (int i = 0; i < grid.getSize(); i++) {
                seriesList.get(seriesList.size() - 1).getData().add(new XYChart.Data<>(grid.getXat(i), grid.getYat(i)));
            }
            chart.getData().add(seriesList.get(seriesList.size() - 1));
            seriesList.get(seriesList.size() - 1).getNode().setStyle(style);
        }
        if (!checkBox.isSelected()) {
            turnVisibility(checkBox, seriesList);
        }
    }
}
