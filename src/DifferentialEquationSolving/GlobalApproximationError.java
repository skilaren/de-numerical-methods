package DifferentialEquationSolving;

import java.util.ArrayList;

/***
 * Global approximation error for given numerical method in range from nStart to nLast with step "+1"
 * Values of errors are stored in globalError attribute
 */
public class GlobalApproximationError {

    private ArrayList<Grid> globalErrors;

    public GlobalApproximationError(Solution numerical, int nStart, int nLast) {
        Grid globalError = new Grid();
        globalErrors = new ArrayList<>();
        globalErrors.add(globalError);
        for (int i = nStart; i <= nLast; i++) {
            // Create new IVP with the correspondent step = i, create the LocalError with it and then get the maximum
            // value of error with this step
            IVP ivp = new IVP(numerical.getIvp().getX0(), numerical.getIvp().getY0(), numerical.getIvp().getX(), i);
            numerical.setIvp(ivp);
            Exact exact = new Exact(ivp);
            ApproximationError numericalError = new ApproximationError(exact, numerical);
            globalError.addPair(i, numericalError.getMaxError());
        }
    }

    public ArrayList<Grid> getGlobalErrors() {
        return globalErrors;
    }
}
