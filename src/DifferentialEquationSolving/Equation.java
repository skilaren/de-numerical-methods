package DifferentialEquationSolving;

import java.util.ArrayList;

/***
 * Equation itself contains logic(arithmetic operations) for computing the right-hand side with given X and Y
 * and logic for computing constant and asymptotes for IVP
 */
class Equation {

    private ArrayList<Double> asymptotes;
    private double constant;

    Equation(double x0, double y0) {
        double exp = Math.pow(Math.E, 3 * x0 * x0 / 2);
        constant = exp * (y0 + 3) / y0;
        asymptotes = new ArrayList<>();
        double firstA = Math.sqrt(Math.log(constant) * 2 / 3);
        double secondA = -firstA;
        asymptotes.add(firstA);
        asymptotes.add(secondA);
    }

    static double compute(double x, double y) {
        return x * y * y + 3 * x * y;
    }

    ArrayList<Double> getAsymptotes() {
        return asymptotes;
    }

    double getConstant() {
        return constant;
    }
}
