package DifferentialEquationSolving;

import java.util.ArrayList;

/****
 * Error of numerical method compared to exact solution
 */
public class ApproximationError {
    private ArrayList<Grid> errorGridList;

    /***
     *
     * @param exact
     * exact solution to compare with
     * @param numerical
     * numerical method you want to compare with exact solution
     */
    public ApproximationError(Exact exact, Solution numerical) {
        errorGridList = new ArrayList<>();
        Grid errorGrid = new Grid();
        errorGridList.add(errorGrid);
        for (Grid exactGrid : exact.getGrids()) {
            // Find the piece of exact solution with x values equal to given numerical
            // then take the absolute value of difference between exact and numerical values and
            // put it to the ErrorGrid
            int index = exact.getGrids().indexOf(exactGrid);
            Grid numericalGrid = numerical.getGrids().get(index);
            for (int i = 0; i < exactGrid.getSize(); i++) {
                double error = Math.abs(exactGrid.getYat(i) - numericalGrid.getYat(i));
                errorGrid.addPair(exactGrid.getXat(i), error);
            }
        }
    }

    public ArrayList<Grid> getErrorGridList() {
        return errorGridList;
    }

    double getMaxError() {
        double maxError = 0;
        for (int i = 0; i < errorGridList.get(0).getSize(); i++) {
            maxError = Math.max(maxError, errorGridList.get(0).getYat(i));
        }
        return maxError;
    }
}
