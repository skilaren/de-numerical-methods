package DifferentialEquationSolving;

/***
 * Exact solution of given Initial Value Problem
 */
public class Exact extends Solution {

    public Exact(IVP ivp) {
        super(ivp);
    }

    @Override
    void calculate() {
        double xNow = ivp.getX0();
        double yNow;
        double step = ivp.getStep();
        while (xNow < ivp.getX()) {
            if (closeToAsymptote(xNow)) {
                xNow += 3 * step;
            } else {
                xNow += step;
                double exp = Math.pow(Math.E, 3 * xNow * xNow / 2);
                yNow = (3 * exp) / (ivp.getEquation().getConstant() - exp);
                grids.get(grids.size() - 1).addPair(xNow, yNow);
            }
        }
    }
}
