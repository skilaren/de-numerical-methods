package DifferentialEquationSolving;

/***
 * Runge-Kutta method of solving differential equation numerically
 */
public class RungeKutta extends Solution {

    public RungeKutta(IVP ivp) {
        super(ivp);
    }

    @Override
    void calculate() {
        double xNow = ivp.getX0();
        double k1, k2, k3, k4;
        double yNow;
        double step = ivp.getStep();
        while (xNow < ivp.getX()) {
            if (closeToAsymptote(xNow)) {
                xNow += 3 * step;
            } else {
                Grid grid = grids.get(grids.size() - 1);
                double xPast = grid.getXat(grid.getSize() - 1);
                double yPast = grid.getYat(grid.getSize() - 1);
                xNow += step;
                k1 = step * Equation.compute(xPast, yPast);
                k2 = step * Equation.compute(xPast + step / 2, yPast + step / 2 * k1);
                k3 = step * Equation.compute(xPast + step / 2, yPast + step / 2 * k2);
                k4 = step * Equation.compute(xPast + step, yPast + step * k3);
                yNow = yPast + (k1 + 2 * k2 + 2 * k3 + k4) / 6;
                grid.addPair(xNow, yNow);
            }
        }
    }
}
