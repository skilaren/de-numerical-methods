package DifferentialEquationSolving;

import java.util.ArrayList;

/***
 * Solution represents a solution to given Initial Value Problem
 * Grid is values computed by this solution
 * IVP is Initial Value Problem itself with given x0, y0, x and h(step)
 */
public abstract class Solution {

    IVP ivp;
    ArrayList<Grid> grids;

    Solution(IVP ivp) {
        setIvp(ivp);
    }

    /***
     * Fills the grid with values computed by this solution
     */
    abstract void calculate();

    public ArrayList<Grid> getGrids() {
        return grids;
    }

    /***
     * returns if this x is close to asymptote or not yet
     * @param x
     * x itself at the moment of computing in the methods
     * @return
     * answer to question "is this x close to asymptote?"
     */
    boolean closeToAsymptote(double x) {
        for (double asymptote : ivp.getEquation().getAsymptotes()) {
            if (x < asymptote && x + ivp.getStep() > asymptote) {
                grids.add(new Grid());
                // If x is close to asymptote we need to get x a little far from discontinuity point
                // to avoid numerical methods go to infinity due to tangent line being too vertical
                double x0 = x + 3 * ivp.getStep();
                double exp = Math.pow(Math.E, 3 * x0 * x0 / 2);
                double y0 = (3 * exp) / (ivp.getEquation().getConstant() - exp);
                grids.get(grids.size() - 1).addPair(x0, y0);
                return true;
            }
        }
        return false;
    }

    IVP getIvp() {
        return ivp;
    }

    /***
     * Here we create and compute the new solutions with given new ivp
     * @param newIvp
     * new ivp itself
     */
    void setIvp(IVP newIvp) {
        this.ivp = newIvp;
        grids = new ArrayList<>();
        grids.add(new Grid());
        grids.get(0).addPair(ivp.getX0(), ivp.getY0());
        calculate();
    }
}
