package DifferentialEquationSolving;

/***
 * IVP with:
 * given start values - x0, y0;
 * final value of needed interval - x;
 * step for computing - step;
 * equation with asymptotes and constant - equation;
 */
public class IVP {
    private double x0;
    private double y0;
    private double x;
    private double step;
    private int stepsNumber;
    private Equation equation;

    public IVP(double x0, double y0, double x, int stepsNumber) {
        this.x0 = x0;
        this.y0 = y0;
        this.x = x;
        this.step = (x - x0) / stepsNumber;
        this.stepsNumber = stepsNumber;
        equation = new Equation(x0, y0);
    }

    double getX0() {
        return x0;
    }

    double getY0() {
        return y0;
    }

    double getX() {
        return x;
    }

    double getStep() {
        return step;
    }

    Equation getEquation() {
        return equation;
    }

    int getStepsNumber() {
        return stepsNumber;
    }
}
