package DifferentialEquationSolving;

import java.util.ArrayList;

/***
 * Class Grid
 * Represents values of graph to be plotted
 */
public class Grid {

    private int size;
    private ArrayList<Double> x;
    private ArrayList<Double> y;

    Grid() {
        x = new ArrayList<>(size);
        y = new ArrayList<>(size);
        this.size = 0;
    }

    public int getSize() {
        return size;
    }

    void addPair(double x, double y) {
        this.x.add(x);
        this.y.add(y);
        size++;
    }

    public double getYat(int i) {
        return y.get(i);
    }

    public double getXat(int i) {
        return x.get(i);
    }

    public ArrayList<Double> getX() {
        return x;
    }

    public ArrayList<Double> getY() {
        return y;
    }
}
