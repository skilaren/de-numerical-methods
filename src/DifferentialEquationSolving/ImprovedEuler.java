package DifferentialEquationSolving;

/***
 * Improved Euler numerical method to solve Initial Value Problem
 */
public class ImprovedEuler extends Solution {

    public ImprovedEuler(IVP ivp) {
        super(ivp);
    }

    @Override
    void calculate() {
        double xNow = ivp.getX0();
        double yNow;
        double yMid;
        double step = ivp.getStep();
        while (xNow <= ivp.getX()) {
            if (closeToAsymptote(xNow)) {
                xNow += 3 * step;
            } else {
                Grid grid = grids.get(grids.size() - 1);
                double xPast = grid.getXat(grid.getSize() - 1);
                double yPast = grid.getYat(grid.getSize() - 1);
                // x'i+1' = x'i' + h
                xNow += step;
                // y'i+1' = y(x'i') + f(x'i', y'i')
                yMid = yPast + step * Equation.compute(xPast, yPast);
                yNow = yPast + step / 2 * (Equation.compute(xPast, yPast) + Equation.compute(xNow, yMid));
                grid.addPair(xNow, yNow);
            }
        }
    }
}
